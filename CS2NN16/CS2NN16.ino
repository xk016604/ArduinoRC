// define pins
#define SW        13    // pin to thumbstick switch
#define VRxPin    14    // pin to thumbstick VRx: up/down
#define VRyPin    15    // pin to thumbstick VRx: left/right

#define ledFR     6
#define ledFL     5
#define ledBR     10
#define ledBL     9

#define ledCheck  11    // pin to check indicator

int ledCheckIntensity;

struct input_limits {
  int _pin;
  int _min, _max;
};

struct input_package {
  input_limits x;
  input_limits y;
};

input_package tStick;

int left, rght, slip;
int check;
int outFL, outBL, outFR, outBR;

struct output_wheel {
  int sign;
  int magnitude;
} wheelLeft, wheelRight;

void setup() {
  Serial.begin(9600); /*Find all> replace //Serial w/ Serial or vice versas*/

  pinMode(SW, INPUT);
  pinMode(VRxPin, INPUT);
  pinMode(VRyPin, INPUT);

  tStick = CalibratePt1();
  Serial.print(tStick.x._min); Serial.print("\t");
  Serial.println(tStick.x._max);
  Serial.print(tStick.y._min); Serial.print("\t");
  Serial.println(tStick.y._max);

}

void loop() {

  rght = analogRead(tStick.x._pin); //Yes i'm replacing input with right to conserve memory
  rght = map(rght, tStick.x._min, tStick.x._max, -263, 263);

  slip = analogRead(tStick.y._pin);
  slip = map(slip, tStick.y._min, tStick.y._max, -263, 263);


  Serial.print(analogRead(tStick.x._pin)); Serial.print("\t");
  Serial.print(analogRead(tStick.y._pin)); Serial.print("\t");

  Serial.print(rght); Serial.print("\t");
  Serial.print(slip); Serial.print("\t");
  
  left = rght + slip;
  rght = rght - slip; //omitting 'in' variable makes this part of the code look wierd, it's as planned.

  Serial.print(left); Serial.print("\t");
  Serial.print(rght); Serial.print("\t");
  
  outFL = constrain(left - 8, 0, 255);
  outBL = constrain( - left - 8, 0, 255);
  outFR = constrain(rght - 8, 0, 255);
  outBR = constrain( - rght - 8, 0, 255);

  Serial.print(outFL); Serial.print("\t");
  Serial.print(outBL); Serial.print("\t");
  Serial.print(outFR); Serial.print("\t");
  Serial.print(outBR); Serial.print("\t");

  check = (outFL + outBL + outFR + outBR == 0) ? 0 :
          (outFL && outFR) ? 1 :
          (outBL && outBR) ? 2 :
          (outFR && outBL) ? 3 :
          (outBR && outFL) ? 4 :
          (outFL) ? 5 :
          (outFR) ? 6 :
          (outBL) ? 7 :
          (outBR) ? 8 : -1;

  Serial.print(check);
  Serial.println();
  if (outFL && outBL) {
    Serial.println("error left");
  }
  if (outFR && outBR) {
    Serial.println("error right");
  }

  analogWrite(ledFL, outFL /** !outBL*/);
  analogWrite(ledBL, /*!outFL **/ outBL);

  analogWrite(ledFR, outFR /** !outBR*/);
  analogWrite(ledBR, /*!outFR **/ outBR);
  //Serial.println(map(analogRead(tStick.x._pin), tStick.x._min, tStick.x._max, -255, 255));
  delay(100);
}

struct input_package CalibratePt1() {
  input_package pack = {{ VRxPin , 0 , 1023 }, { VRyPin , 0 , 1023 }};
  int x_val, y_val;
  unsigned int x_mid = 0, y_mid = 0;

  // [Take {64} samples @ potentionmeter] sample for average middle value

  pinMode(ledCheck, OUTPUT);

  for (int i = 1; i < 65; i++) {  //LOOP  64 times

    x_val = analogRead(pack.x._pin);  //INPUT read pins x, y to value
    y_val = analogRead(pack.y._pin);

    x_mid = x_mid + x_val;            //MATH  total all of the samples to uint
    y_mid = y_mid + y_val;

    ledCheckIntensity = i * 4 - 1;
    analogWrite(ledCheck, ledCheckIntensity);

    delay(16);
    //Be aware that Serial.print will delay the time
  }
  pinMode(ledCheck, INPUT);

  x_mid = (x_mid + 32) / 64;          //MATH  get the midpoint
  y_mid = (y_mid + 32) / 64;

  Serial.print(x_mid); Serial.print("\t"); Serial.println(y_mid);

  pack.x = CalibratePt2(pack.x, x_mid); //FUNC  set limits
  pack.y = CalibratePt2(pack.y, y_mid);

  return {pack.x, pack.y};
}

struct input_limits CalibratePt2(input_limits axis, int mid)  {
  int low = 1023 - mid;
  mid < low ? axis._max = mid + mid : axis._min = mid - low;
  //  if (mid < low) {        //GET lowest range
  //    low = mid;
  //    axis._max = mid + low;//SET smaller maximum
  //  } else {
  //    axis._min = mid - low;//OR SET  bigger minimum
  //  }
  return {axis._pin, axis._min, axis._max};
}

